FROM debian:stretch
MAINTAINER Nicola Corna <nicola@corna.info>

# Environment variables
#######################

ENV SRC_DIR /srv/src
ENV CCACHE_DIR /srv/ccache
ENV APK_DIR /srv/apk
ENV USERSCRIPTS_DIR /srv/userscripts
ENV PATH "$PATH:/opt/sdk-tools/tools/bin/:/opt/sdk-tools/build-tools/29.0.2/"

ENV DEBIAN_FRONTEND noninteractive
ENV USER root

# Configurable environment variables
####################################

# By default we want to use CCACHE, you can disable this
# WARNING: disabling this may slow down a lot your builds!
ENV USE_CCACHE 1

# ccache maximum size. It should be a number followed by an optional suffix: k,
# M, G, T (decimal), Ki, Mi, Gi or Ti (binary). The default suffix is G. Use 0
# for no limit.
ENV CCACHE_SIZE 50G

# Environment for the LineageOS branches name
# See https://github.com/LineageOS/android_vendor_cm/branches for possible options
ENV BRANCH_NAME 'cm-14.1'

# Repo use for build
ENV REPO 'https://github.com/LineageOS/android.git'

# User identity
ENV USER_NAME 'LineageOS Buildbot'
ENV USER_MAIL 'lineageos-buildbot@docker.host'

# Clean artifacts output after each build
ENV CLEAN_AFTER_BUILD true
# Provide a default JACK configuration in order to avoid out-of-memory issues
ENV ANDROID_JACK_VM_ARGS "-Dfile.encoding=UTF-8 -XX:+TieredCompilation -Xmx4G"

# Path to the source code of the module to build
ENV MODULE_PATH packages/apps/Settings

# Module name (defined in the makefile)
ENV MODULE_NAME Settings

# When the app is defined as a priv app
ENV PRIV_APP true

# Create Volume entry points
############################
VOLUME $SRC_DIR
VOLUME $CCACHE_DIR
VOLUME $APK_DIR
VOLUME $USERSCRIPTS_DIR
VOLUME /root/.ssh

# Copy required files
#####################
COPY src/ /root/

# Create missing directories
############################
RUN mkdir -p $SRC_DIR
RUN mkdir -p $CCACHE_DIR
RUN mkdir -p $APK_DIR
RUN mkdir -p $USERSCRIPTS_DIR

# Install build dependencies
############################
COPY apt_preferences /etc/apt/preferences
RUN apt-get -qq update
RUN apt-get install -y imagemagick libwxgtk3.0-dev openjdk-8-jdk

RUN echo 'deb http://deb.debian.org/debian sid main' >> /etc/apt/sources.list
RUN echo 'deb http://deb.debian.org/debian experimental main' >> /etc/apt/sources.list
RUN apt-get -qq update
RUN apt-get -qqy upgrade

RUN apt-get install -y bc bison bsdmainutils build-essential ccache cgpt cron \
      curl flex g++-multilib gcc-multilib git gnupg gperf imagemagick kmod \
      lib32ncurses5-dev lib32readline-dev lib32z1-dev libesd0-dev liblz4-tool \
      libncurses5-dev libsdl1.2-dev libssl-dev libxml2 \
      libxml2-utils lsof lzop maven pngcrush \
      procps python rsync schedtool squashfs-tools wget xdelta3 xsltproc yasm \
      zip zlib1g-dev

RUN curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo
RUN chmod a+x /usr/local/bin/repo

RUN wget -O /tmp/sdk-tools.zip https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip
RUN unzip /tmp/sdk-tools.zip -d /opt/sdk-tools
RUN mkdir -p /opt/sdk-tools/licenses/ \
  && echo "24333f8a63b6825ea9c5514f83c2829b004d1fee" > /opt/sdk-tools/licenses/android-sdk-license
RUN /opt/sdk-tools/tools/bin/sdkmanager --update && touch ~/.android/repositories.cfg && /opt/sdk-tools/tools/bin/sdkmanager --install "build-tools;29.0.2"
RUN rm /tmp/sdk-tools.zip

# Set the work directory
########################
WORKDIR $SRC_DIR

# Allow redirection of stdout to docker logs
############################################
RUN ln -sf /proc/1/fd/1 /var/log/docker.log

# Set the entry point to init.sh
################################
ENTRYPOINT /root/init.sh
