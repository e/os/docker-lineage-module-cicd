#!/bin/bash

# Docker build script
# Copyright (c) 2017 Julian Xhokaxhiu
# Copyright (C) 2017-2018 Nicola Corna <nicola@corna.info>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# cd to working directory
cd "$SRC_DIR"

if [ -f /root/userscripts/begin.sh ]; then
  echo ">> [$(date)] Running begin.sh"
  /root/userscripts/begin.sh
fi

for branch in ${BRANCH_NAME//,/ }; do
  branch_dir=$(sed 's/.*-\([a-zA-Z]*\)$/\1/' <<< $branch)
  branch_dir=${branch_dir^^}

  if [ -n "$branch" ] ; then

    mkdir -p "$SRC_DIR/$branch_dir"
    cd "$SRC_DIR/$branch_dir"

    echo ">> [$(date)] Branch:  $branch"
    echo ">> [$(date)] Module: $MODULE_NAME (under $MODULE_PATH)"

    # Remove previous changes of vendor/cm, vendor/lineage and frameworks/base (if they exist)
    for path in "vendor/cm" "vendor/lineage" "frameworks/base"; do
      if [ -d "$path" ]; then
        cd "$path"
        git reset -q --hard
        git clean -q -fd
        cd "$SRC_DIR/$branch_dir"
      fi
    done

    echo ">> [$(date)] (Re)initializing branch repository"
    yes | repo init -u "$REPO" -b "$branch"

    echo ">> [$(date)] Syncing branch repository"
    builddate=$(date +%Y%m%d)
    repo sync -c -d --force-sync

    if [ $? != 0 ]; then
      sync_successful=false
    fi

    android_version=$(sed -n -e 's/^\s*PLATFORM_VERSION\.OPM1 := //p' build/core/version_defaults.mk)
    if [ -z $android_version ]; then
      android_version=$(sed -n -e 's/^\s*PLATFORM_VERSION\.PPR1 := //p' build/core/version_defaults.mk)
      if [ -z $android_version ]; then
        android_version=$(sed -n -e 's/^\s*PLATFORM_VERSION := //p' build/core/version_defaults.mk)
        if [ -z $android_version ]; then
          echo ">> [$(date)] Can't detect the android version"
          exit 1
        fi
      fi
    fi
    android_version_major=$(cut -d '.' -f 1 <<< $android_version)

    # if [ "$android_version_major" -ge "8" ]; then
    #   vendor="lineage"
    # else
    #   vendor="cm"
    # fi
    #
    # if [ ! -d "vendor/$vendor" ]; then
    #   echo ">> [$(date)] Missing \"vendor/$vendor\", aborting"
    #   exit 1
    # fi
    #
    # # Set up our overlay
    # mkdir -p "vendor/$vendor/overlay/microg/"
    # sed -i "1s;^;PRODUCT_PACKAGE_OVERLAYS := vendor/$vendor/overlay/microg\n;" "vendor/$vendor/config/common.mk"


    # if [ "$SIGN_BUILDS" = true ]; then
    #   echo ">> [$(date)] Adding keys path ($KEYS_DIR)"
    #   # Soong (Android 9+) complains if the signing keys are outside the build path
    #   ln -sf "$KEYS_DIR" user-keys
    #   sed -i "1s;^;PRODUCT_DEFAULT_DEV_CERTIFICATE := user-keys/releasekey\nPRODUCT_OTA_PUBLIC_KEYS := user-keys/releasekey\nPRODUCT_EXTRA_RECOVERY_KEYS := user-keys/releasekey\n\n;" "vendor/$vendor/config/common.mk"
    # fi

    if [ "$android_version_major" -ge "7" ]; then
      jdk_version=8
    elif [ "$android_version_major" -ge "5" ]; then
      jdk_version=7
    else
      echo ">> [$(date)] ERROR: $branch requires a JDK version too old (< 7); aborting"
      exit 1
    fi

    echo ">> [$(date)] Using OpenJDK $jdk_version"
    update-java-alternatives -s java-1.$jdk_version.0-openjdk-amd64 &> /dev/null

    # Prepare the environment
    echo ">> [$(date)] Preparing build environment"
    source build/envsetup.sh > /dev/null

    if [ -f /root/userscripts/before.sh ]; then
      echo ">> [$(date)] Running before.sh"
      /root/userscripts/before.sh
    fi

    source_dir="$SRC_DIR/$branch_dir"
    cd "$source_dir"

    if [ -f /root/userscripts/pre-build.sh ]; then
      echo ">> [$(date)] Running pre-build.sh"
      /root/userscripts/pre-build.sh
    fi



    if [ -n ${CI_COMMIT_SHA} ] ; then
      echo ">> [$(date)] Checkout ${CI_COMMIT_SHA} onto ${MODULE_PATH}"
      cd ${MODULE_PATH}
      git fetch e && git checkout $CI_COMMIT_SHA
      cd "$source_dir"
    fi

    # Start the build
    echo ">> [$(date)] Starting build for $MODULE_NAME, $branch branch"
    echo "ANDROID_JACK_VM_ARGS=${ANDROID_JACK_VM_ARGS}"
    if mmma $MODULE_PATH ; then

      # Move produced ZIP files to the main OUT directory
      echo ">> [$(date)] Moving build artifacts for $MODULE_NAME to '$APK_DIR'"
      cd "$source_dir"
      if [ "${PRIV_APP}" = true ]; then
        cd out/target/product/generic/system/priv-app/${MODULE_NAME}/
      else
        cd out/target/product/generic/system/app/${MODULE_NAME}/
      fi
      ls
      mv ${MODULE_NAME}.apk $APK_DIR/
      cd "$source_dir"
    else
      echo ">> [$(date)] Failed build for $MODULE_NAME"
    fi


    if [ -f /root/userscripts/post-build.sh ]; then
      echo ">> [$(date)] Running post-build.sh for $MODULE_NAME"
      /root/userscripts/post-build.sh
    fi
    echo ">> [$(date)] Finishing build for $MODULE_NAME"


    if [ "$CLEAN_AFTER_BUILD" = true ]; then
      echo ">> [$(date)] Cleaning source dir"
      cd "$source_dir"
      mka clean
    fi

  fi
done

if [ -f /root/userscripts/end.sh ]; then
  echo ">> [$(date)] Running end.sh"
  /root/userscripts/end.sh
fi

if [ "$build_successful" = false ] || [ "$sync_successful" = false ]; then
  exit 1
fi
